# Descripcion

Este proyecto consiste en desarrollar un sistema para inspeccionar Vinted.es, obtener una muestra del estilo de ropa del usuario y a partir de esto recomendar al usuario una seleccion de items potencialmente interesantes para el.

El objetivo es lograr esto mediante la implementación de un clasificador basado en redes neurales para saber que prendas pueden gustar mas al usuario. Para ello, es nesario entrenar este clasificador.

El uso tipico seria algo asi:
1. Ofrecer al usuario una lista de items y que este los valore
2. Entrenar el clasificador
3. Inspeccionar items de Vinted con el clasificador entrenado
4. Mostrar estos nuevos items al usuario


